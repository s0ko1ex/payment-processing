const { Sequelize, DataTypes, Op } = require('sequelize');
const { default: isURL } = require('validator/lib/isURL');
const { XMLParser, XMLValidator } = require('fast-xml-parser');
const https = require('https')
const url = require("url")
const path = require("path");
const fs = require("fs")
const fsPromices = require("fs/promises")
const xss = require("xss")
const { v4: uuidv4 } = require('uuid');
const { randomInt } = require('crypto');
const mustache = require('mustache');

class PaymentProcessing {
    constructor(db_url, base_path) {
        this.#sequelize = new Sequelize(db_url);
        this.#base_path = base_path;
        this.#parser = new XMLParser();

        this.#Users = this.#sequelize.define("Users", {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            company: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            companyRole: {
                type: DataTypes.ENUM("CEO", "ACCOUNTANT"),
                allowNull: false,
            },
            limit: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
        })

        this.#Transactions = this.#sequelize.define("Transactions", {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            ownerId: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            authorized: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: true,
            }
        })

        this.#Payments = this.#sequelize.define("Payments", {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            transactionId: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            sender: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            recepient: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            sum: {
                type: DataTypes.INTEGER,
                allowNull: false,
                validate: {
                    min: 1,
                },
            },
            comment: {
                type: DataTypes.STRING(1000),
            },
        })

        this.#Files = this.#sequelize.define("Files", {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            ownerId: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            name: {
                type: DataTypes.STRING(1000),
                allowNull: false,
            },
            path: {
                type: DataTypes.STRING(1000),
                allowNull: false,
            },
        })

        this.#OTP = this.#sequelize.define("OTP", {
            transactionId: {
                type: DataTypes.INTEGER,
                unique: true,
            },
            code: {
                type: DataTypes.STRING(6),
                allowNull: false,
            }
        })

        this.#sequelize.sync();
    }

    async get_payments(user, file) {
        var transactionId = null;
        if (typeof(file) == "string" && isURL(file)) {
            transactionId = await this.#get_payments_url(user, file);
        } else if (typeof(file) == "string") {
            transactionId = await this.#get_payments_path(user, file);
        } else if (file.constructor == Buffer || XMLValidator.validate(file.toString()) == true) {
            transactionId = await this.#get_payments_content(user, file.toString());
        }

        if (transactionId == null) {
            return null;
        }

        this.#OTP.create({
            transactionId: transactionId,
            code: this.#generate_otp_code(),
        });

        return transactionId;
    }

    async #get_payments_content(user, file_content) {
        if (XMLValidator.validate(file_content) != true) {
            return null;
        }

        const user_limit = (await this.#Users.findOne({where: {id: user}})).limit;
        var data = this.#parser.parse(file_content).payments;

        if (!this.#is_sender_correct(data)) {
            return null;
        }

        var cur_sum = this.#get_cur_sum(data);
        var g_payment = null;

        var transaction = (await this.#Transactions.create({
            ownerId: user,
            authorized: this.#get_user_sum(user) + cur_sum <= user_limit
        })).id;

        for (const [key, value] of Object.entries(data)) {
            if (value.constructor === Array) {
                for (const payment of value) {
                    g_payment =  this.#sanitize_payment(payment);
                    if (g_payment == null) {
                        continue
                    }
                    g_payment["transactionId"] = transaction;
                    this.#Payments.create(g_payment);
                }

                continue;
            }

            g_payment =  this.#sanitize_payment(value);
            if (g_payment == null) {
                continue
            }
            g_payment["transactionId"] = transaction;
            this.#Payments.create(g_payment);
        }

        return transaction;
    }

    async #get_payments_url(user, file_url) {
        const path_name = path.basename(url.parse(file_url).pathname);
        const actual_path = path.join(this.#base_path, uuidv4());
        const file = fs.createWriteStream(actual_path);

        https.get(file_url, response => {
            var out = ""

            response.on("data", chunk => {
                out += chunk.toString();
                file.write(chunk);
            })

            response.on("end", () => {
                file.close();
                this.#Files.create({
                    ownerId: user,
                    name: path_name,
                    path: actual_path,
                }).then(() => {
                    return this.get_payments_content(user, out);
                })
            })
        })
    }

    async #get_payments_path(user, file_path) {
        var file = await this.#Files.findOne({
            where: {
                name: file_path,
            },
        });

        if (file == null || file.ownerId != user) {
            return null;
        }

        const data = await fsPromices.readFile(file.path);
        return this.#get_payments_content(data.toString());
    }

    async show_payments(user, transaction) {
        if (!this.#is_transaction_owner(user, transaction)) {
            return null;
        }

        const payments = await this.#Payments.findAll({
            where: {
                transactionId: transaction,
            },
        });
        var new_payments = []
        for (const payment of payments) {
            new_payments.push(this.#sanitize_payment(payment));
        }

        const template = await fsPromices.readFile("template.html");
        return mustache(template.toString(), {payments: new_payments});
    }

    async check_otp(user, transaction, otp) {
        if (!this.#is_transaction_owner(user, transaction)) {
            return false;
        }

        var true_otp = await this.#OTP.findOne({
            where: {
                transactionId : transaction,
            },
        });

        if (true_otp == null) {
            return false;
        } else if (true_otp.code == otp) {
            this.#Transactions.destroy({
                where: {
                    transactionId: transaction,
                },
            });

            return true;
        }

        return false;
    }

    async perform_transaction(user, transaction) {
        if (!this.#is_transaction_owner(user, transaction)) {
            return false;
        }

        if (!(await this.#Transactions.findOne({where: {id: transaction}}).authorized)) {
            return;
        }

        var payments = await this.#Payments.findAll({
            where: {
                transactionId: transaction,
            },
        });

        for (const payment of payments) {
            this.#sequelize.query("CALL do_transfer(:sender, :receiver, :amount)", {
                replacements: {
                    sender: payment.sender,
                    receiver: payment.receiver,
                    amount: payment.amount,
                },
            });
        }
    }

    #get_cur_sum(data) {
        var cur_sum = 0;
        var g_payment = null;

        for (const [key, value] of Object.entries(data)) {
            if (value.constructor == Array) {
                for (const payment of value) {
                    g_payment =  this.#sanitize_payment(payment);
                    if (g_payment == null) {
                        continue
                    }
                    cur_sum += g_payment.sum;
                }

                continue;
            }

            g_payment =  this.#sanitize_payment(value);
            if (g_payment == null) {
                continue
            }
            cur_sum += g_payment.sum;
        }

        return cur_sum;
    }

    async #get_user_sum(user) {
        const TODAY_START = new Date().setHours(0, 0, 0, 0);

        var transactions = await this.#Transactions.findAll({
            where: {
                ownerId: user,
                createdAt: {
                    [Op.lt]: TODAY_START,
                },
            },
        });

        var sum = 0;
        for (const transaction of transactions) {
            sum += await this.#get_transaction_sum(transaction.id);
        }
        return sum;
    }

    async #is_sender_correct(user) {
        const user_company = (await this.#Users.findOne({
            where: {
                id: user
            }
        })).company;

        var g_payment = null;

        for (const [key, value] of Object.entries(data)) {
            if (value.constructor == Array) {
                for (const payment of value) {
                    g_payment =  this.#sanitize_payment(payment);
                    if (g_payment == null || g_payment.sender != user_company) {
                        return false;
                    }
                }

                continue;
            }

            g_payment =  this.#sanitize_payment(value);
            if (g_payment == null || g_payment.sender != user_company) {
                return false;
            }
        }

        return true;
    }

    async #get_transaction_sum(transaction) {
        return this.#Payments.sum("sum", {
            where: {
                transactionId: transaction,
            },
        });
    }

    async #is_transaction_owner(user, transaction) {
        var true_owner = await this.#Transactions.findOne({
            where: {
                id: transaction,
            },
        });
        
        return true_owner != null && true_owner.ownerId == user;
    }

    #sanitize_payment(payment_data) {
        if (payment_data.sender == null || payment_data.recepient == null || payment_data.sum == null || payment_data.sum <= 0) {
            return null;
        }
        
        return {
            sender: payment_data.sender,
            recepient: payment_data.recepient,
            sum: payment_data.sum,
            comment: xss(payment_data.comment),
        };
    }

    #generate_otp_code() {
        var out = "";
        for (var i = 0; i < 6; i++) {
            out += randomInt(0, 10).toString();
        }
        return out;
    }
}

exports.PaymentProcessing = PaymentProcessing;